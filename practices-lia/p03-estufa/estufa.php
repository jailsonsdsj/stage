<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LIA | Determinação da massa seca e do índice de absorção d`água </title>

    <!-- substituir quando estiver pronto
    <link rel="stylesheet" href="../../../css/main.css"> -->
    <!-- <link rel="stylesheet" href="css/main.css"> -->
    <!--css próprio-->
    
    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="stylesheet" href="https://lia-labv.s3.sa-east-1.amazonaws.com/teste/p03-estufa/css/style.css"> -->
</head>
<body>

    <!--CANVAS-->
    <div id="canvas"></div>

   
    
</body>
<script src="https://cdn.jsdelivr.net/npm/pixi.js-legacy@5.3.7/dist/pixi-legacy.min.js" integrity="sha256-znxy9dcwi+Q0vCzCtoZ6mlsdFMezt7m/IlDLqSvH060=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/gsap@3.6.0/dist/gsap.min.js" integrity="sha256-0+1stGa9ZU/jbFf6rLHIitTieTCHQx1v/FNmlhpMl48=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/gsap@3.6.0/dist/PixiPlugin.min.js" integrity="sha256-+kuGJv7BiwEeMgQQ+idx03aARIMZzt2q5uHO4bcct8c=" crossorigin="anonymous"></script>
<script type="text/javascript">
    const PIXILegacy = PIXI;
</script>
<script src="https://cdn.jsdelivr.net/npm/pixi.js@5.3.7/dist/pixi.min.js" integrity="sha256-kraYJiQN21Z9BbGRczdjvDL7nznrJohivxvUMOgJj8o=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/fontfaceobserver@2.1.0/fontfaceobserver.standalone.js" integrity="sha256-fhre4iYmFiwPsIkJajvUGcWlfVAEPjG/bnsuopL2NB8=" crossorigin="anonymous"></script>

<script src="js/uikit.js"></script>
<script src="js/setup-v3.js"></script>
<script src="js/intro2.js"></script>
<script src="js/animation.js"></script>

<!--
<script src="https://lia-labv.s3.sa-east-1.amazonaws.com/teste/p03-estufa/js/uikit.js"></script>
<script src="https://lia-labv.s3.sa-east-1.amazonaws.com/teste/p03-estufa/js/setup-v3.js"></script>
-->
<!--CARREGA ARQ. DE FUNÇÕES AQUI-->
<!--
<script src="https://lia-labv.s3.sa-east-1.amazonaws.com/teste/p03-estufa/js/animation.js"></script>
<script src="https://lia-labv.s3.sa-east-1.amazonaws.com/teste/p03-estufa/js/virtual-lab.js"></script>
-->

</html>