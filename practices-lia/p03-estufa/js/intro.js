//MAIN
const introOptions = {
    resources: {
        logoCircle:
            "https://lia-labv.s3.sa-east-1.amazonaws.com/praticas/pratica-abertura/circulo.png",
        logoLamp:
            "https://lia-labv.s3.sa-east-1.amazonaws.com/praticas/pratica-abertura/lampada.png",
        logoLines:
            "https://lia-labv.s3.sa-east-1.amazonaws.com/praticas/pratica-abertura/piscas.png",
        progressBar:
            "https://lia-labv.s3.sa-east-1.amazonaws.com/praticas/pratica-abertura/Cinza+arredondado.png",
        progress:
            "https://lia-labv.s3.sa-east-1.amazonaws.com/LIA/%C3%ADcones/pngtree-gaming-blue-technology-sense-progress-bar-png-image_2823080__1_-removebg-preview.png",
    },
};

const intro = (app, resources) => {
    /*logx-imagem, wx-width,hx-height ,x,y*/
    const configObject = (logx, wx, hx, x, y) => {
        logx.width = wx;
        logx.height = hx;
        logx.x = x;
        logx.y = y;
        logx.pivot.set = (logx.width / 2, logx.height / 2);
        app.stage.addChild(logx);
    };

    let introBackground = new PIXI.Graphics();
    introBackground.beginFill(0x050A30);
    introBackground.lineStyle(5, 0x000000);
    introBackground.drawRect(0, 0, 800, 600);

    app.stage.addChild(introBackground);
    //logo
    const logoCircle = new PIXI.Sprite(resources.logoCircle.texture);

    configObject(logoCircle, 260, 260, 272, 600);

    //criar uma função para configurar automaticamente os objetos (a função deve seguir o conceito de hosting)

    const logoLamp = new PIXI.Sprite(resources.logoLamp.texture);
    configObject(logoLamp, 150, 180, 325, 600);

    const logoLines = new PIXI.Sprite(resources.logoLines.texture);
    configObject(logoLines, 50, 50, 800, 75);

    //A FUNÇÃO NÃO PRECISA SER APLICADA AOS TEXTOS
    //title
    const titleText = new PIXI.Text("Laboratório de Inovações Acadêmicas");
    titleText.x = 180;
    titleText.y = 420;

    titleText.style = new PIXI.TextStyle({
        fill: 0xffffff,
    });
    app.stage.addChild(titleText);
    titleText.alpha = 0;

    //progress
    const progressBar = new PIXI.Sprite(resources.progressBar.texture);
    configObject(progressBar, 600, 30, 100, 470);
    progressBar.alpha = 0;

    const progress = new PIXI.Sprite(resources.progress.texture);
    configObject(progress, 0, 30, 100, 470);
    progress.alpha = 0;


    

    //animation

    //CRIAR UMA FUNÇÃO PARA A ANIMAÇÃO QUE RETORNE A TIME LINE
    //CHAMAR A FUNÇÃO NO FINAL
    function animation() {
        let blocks = gsap.timeline({
            delay: 1,
        });
        blocks.to([progress, progressBar, titleText], {
            pixi: {
                alpha: 1,
            },
            duration: 1,
        });
        blocks.to(logoCircle, {
            pixi: {
                y: 50,
            },
            delay: 0.3,
            duration: 0.2,
            visible: true,
        });

        blocks.to(logoLamp, {
            pixi: {
                y: 100,
            },
            delay: 0.5,
            duration: 0.1,
            visible: true,
        });

        blocks.to(logoLines, {
            pixi: {
                x: 450,
            },
            delay: 0.3,
            duration: 0.1,
            visible: true,
        });

        blocks.to(progress, {
            pixi: {
                width: 600,
            },
            delay: 0.3,
            duration: 1.5,
            visible: true,
        });

        blocks.to(
            [logoLamp, logoLines, logoCircle, progress, titleText, progressBar, introBackground],
            {
                pixi: {
                    alpha: 0,
                },
                delay: 1,
            }
        );
        return blocks;
    }

    return animation();
}
