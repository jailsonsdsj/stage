<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/uikit.css">
    <link rel="stylesheet" href="css/main.css">
    <title>Página Inicial</title>
</head>

<body>
    <!--HEADER-->
<?php include_once('_include/header.php'); ?>
    <main>
        <section class="wellcome-message">
           
            <div class="message-text uk-animation-slide-bottom-medium">
                <h3>Bem vindo, Nome do Aluno! </h3>
                <p >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla egestas maximus risus, id convallis nisl. Nunc eget pharetra nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras in tincidunt mauris. Sed in varius sem, a condimentum odio. Interdum et malesuada fames ac ante ipsum primis.</p>
            </div>
            <img id="logo-larger" class="img-fluid" src="icons/logo-branca.png">
        </section>
    </main>
<!--Footer and Scripts-->
<?php include_once('_include/footer.php'); ?>
</body>




</html>