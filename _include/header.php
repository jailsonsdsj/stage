
<header>
    <div class="menu uk-animation-slide-right">
        <ul class="navbar">
            <a href="inicio.php">
                <li>Página Inicial</li>
            </a>
            <a href="sobre.php">
                <li>Sobre o Laboratório Virtual</li>
            </a>
        </ul>
        <div class="profile-panel">
            <span>Olá, Heloísa!</span>
        </div>
        <div uk-navbar>
            <a href="#" class="uk-active">
                <img class="icon" src="icons/user.svg">
            </a>
            <div class="uk-navbar-dropdown">
                <div class="mobile-profile-panel">
                   Olá, Nome do Aluno!
                   <hr>
                </div>
                
                <ul class="uk-nav uk-navbar-dropdown-nav">
                    <li><a href="#">Meu Perfil</a></li>
                    <li><a href="#">Sair</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="menu-toggle-bar">
        <a href="#offcanvas-usage" uk-toggle><img id="icon-menu" class="icon" src="icons/align-justify.svg"></a>
    </div>

    <div id="offcanvas-usage" uk-offcanvas>
        <div class="uk-offcanvas-bar">
            <button class="uk-offcanvas-close" type="button" uk-close></button></span>
            <!--mobile menu-->
            <div class="mobile-menu">
                <ul class="mobile-navbar">
                    <a href="inicio.php"><li>Página Inicial</li></a>
                    <a href="sobre.php"><li>Sobre o Laboratório Virtual</li></a>
                </ul>
                <hr>
            </div>
            
            <!--Courses area-->
            <ul class="uk-nav-primary uk-nav-parent-icon" uk-nav>
                <h3>Área de Interesse</h3>
                <li class="uk-parent">
                    <a href="#">Exatas</a>
                    <ul class="uk-nav-sub">
                        <li><a href="#">Arquitetura</a></li>
                        <li><a href="#">Ciclo Básico</a></li>
                        <li><a href="praticas-engenharia-civil.php">Engenharia Civil</a></li>
                        <li><a href="#">Engenharia Elétrica</a></li>
                        <li><a href="#">Engenharia Mecânica</a></li>
                        <li><a href="#">Engenharia de Produção</a></li>
                    </ul>
                </li>
                <li class="uk-parent">
                    <a href="#">Educação</a>
                    <ul class="uk-nav-sub">
                        <li><a href="#">Letras - espanhol</a></li>
                        <li><a href="#">Letras - Inglês</a></li>
                        <li><a href="#">Letras - português</a></li>
                        <li><a href="#">Pedagogia</a></li>
                    </ul>
                </li>
                <li class="uk-parent">
                    <a href="#">Humanas</a>
                    <ul class="uk-nav-sub">
                        <li><a href="#">Administração</a></li>
                        <li><a href="#">Ciências contábeis</a></li>
                        <li><a href="#">Ciências Econômicas</a></li>
                        <li><a href="#">Marketing</a></li>
                        <li><a href="#">Psicologia</a></li>
                        <li><a href="#">Serviço Social</a></li>

                    </ul>
                </li>
                <li class="uk-parent">
                    <a href="#">Saúde</a>
                    <ul class="uk-nav-sub">
                        <li><a href="#">Biomedicina</a></li>
                        <li><a href="#">Educação Física</a></li>
                        <li><a href="#">Estética e Cosméticos</a></li>
                        <li><a href="#">Farmácia</a></li>
                        <li><a href="#">Nutrição</a></li>
                        <li><a href="#">Radiologia</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

</header>