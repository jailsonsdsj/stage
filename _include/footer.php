<footer>
    <img id="footer-logo" src="icons/logo-branca.png" alt="">
    
    <img class="footer-icon" src="icons/instagram.svg">
    <img class="footer-icon" src="icons/facebook.svg">
    <img class="footer-icon" src="icons/youtube.svg">
    <img class="footer-icon" src="icons/linkedin.svg">
</footer>
<script src="js/uikit.js"></script>
<script src="js/uikit.min.js"></script>
<script src="js/uikit-icons.js"></script>
<script src="js/uikit-icons.min.js"></script>
<script src="js/main.js"></script>