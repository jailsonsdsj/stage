<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/uikit.css">
    <link rel="stylesheet" href="css/main.css">
    <title>MODELO</title>
</head>
<!--HEADER-->
<?php include_once('_include/header.php'); ?>
<body>
    

</body>
<!--Footer and Scripts imported-->
<?php include_once('_include/footer.php'); ?>
<script src="js/main.js"></script>
<script src="js/uikit.js"></script>
<script src="js/uikit.min.js"></script>
<script src="js/uikit-icons.js"></script>
<script src="js/uikit-icons.min.js"></script>
</html>